package zdanie18;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Pierwiastek {
    public static void main(String[] args) throws IOException {
        double a, b, c, delta, x1, x2;

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println(" Program oblicza pierwiastki równania ax^2 + bx = 0 " + "\n" + " Podaj wartość dla a: ");

        a = Double.parseDouble(bufferedReader.readLine());

        if (a == 0){
            System.out.println("Niedozwolona wartość dla a.");
        }else {
            System.out.println(" Podaj wartość b: ");
            b = Double.parseDouble(bufferedReader.readLine());
            System.out.println(" Podaj wartość c: ");
            c = Double.parseDouble(bufferedReader.readLine());

            delta = (b * b) - (4 * a *c);

            if (delta < 0){
                System.out.println(" Brak pierwiastków rzeczywistych.");
            }else {
            }
            if (delta == 0){
                x1 = - b/(2*a);
                System.out.printf(" Dla a = " + "%4.2f,", a);
                System.out.printf(" b = " + "%4.2f,", b);
                System.out.printf(" c = " + "%4.2f,", c);
                System.out.printf(" trójmian ma jeden pierwiastek x1 =" + "%4.2f, x1");

            }else {
                x1 = (-b-Math.sqrt(delta))/(2*a);
                x2 = (-b+Math.sqrt(delta))/(2*a);
                System.out.printf(" Dla a = " + "%4.2f,", a);
                System.out.printf(" b = " + "%4.2f,", b);
                System.out.printf(" c = " + "%4.2f,", c);
                System.out.printf(" trójmian ma dwa pierwiastki x1 =" + "%4.2f,", x1);
                System.out.printf(" x2 = " + "%4.2f,", x2);
            }
        }
    }
}

