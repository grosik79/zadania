package zadanie15;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Dzielenie {
    public static void main(String[] args)throws IOException {

        int a,b;
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Program będzie wykonywał dzielenie.");
        System.out.println(" Podaj proszę liczbę a: ");
        a = Integer.parseInt(bufferedReader.readLine());
        System.out.println(" Podaj proszę liczbę b: ");
        b = Integer.parseInt(bufferedReader.readLine());

        System.out.println("Wynik dzielenia podanych liczb: " + a/b);
        System.out.println("Reszta z dzielenia podanych liczb:" + a%b);
    }
}
