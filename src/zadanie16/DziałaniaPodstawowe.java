package zadanie16;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class DziałaniaPodstawowe {
    public static void main(String[] args)throws IOException {
        float a,b,c, suma, roznica, iloczyn, iloraz;

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println(" Podaj proszę liczbę a");
        a = Float.parseFloat(bufferedReader.readLine());
        System.out.println(" Podaj proszę liczbę b");
        b = Float.parseFloat(bufferedReader.readLine());
        System.out.println(" Podaj proszę liczbę c");
        c = Float.parseFloat(bufferedReader.readLine());

        suma = a + b;
        roznica = c - a;
        iloczyn = b * c;
        iloraz = c / b;

        System.out.printf(" Sumą podanych liczb: " + "%2.2f\n", suma);
        System.out.printf(" Różnicą podanych liczb: " + "%2.2f\n", roznica);
        System.out.printf(" Iloczyn podanych liczb: " + "%2.2f\n",iloczyn);
        System.out.printf(" Iloraz podanych liczb: " + "%2.2f\n",iloraz);
    }
}
