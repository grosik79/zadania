package zadanie11;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class PoleProstokąta {
    public static void main(String[] args)
            throws IDException, IOException {
            double a, b, pole;
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Program oblicza pole prostokąta");
        System.out.println("Podaj bok a:");
        a = Double.parseDouble(bufferedReader.readLine());
        System.out.println("Podaj bok b:");
        b = Double.parseDouble(bufferedReader.readLine());
        pole = a * b;

        System.out.println("Pole prostokąta o boku a:" + a + " i boku b:" + b + " wynosi: " + pole);

        }
    }
