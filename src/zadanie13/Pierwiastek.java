package zadanie13;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Pierwiastek {
    public static void main(String[] args) throws IOException {
        double c;
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Program liczy pierwiastek z zadanej c liczby");
        System.out.println("Podaj liczbę c: ");
        c = Double.parseDouble(bufferedReader.readLine());
        System.out.printf("Pierwiastek z liczby c: " + "%2.2f\n", Math.sqrt(c));

    }
}
