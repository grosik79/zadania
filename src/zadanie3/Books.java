package zadanie3;

public class Books {
    public String title;
    public String author;
    public double publicationDate;

    public Books(String title, String author, double publicationDate) {
        this.title = title;
        this.author = author;
        this.publicationDate = publicationDate;
    }

    @Override
    public String toString() {
        return "Books{" +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", publicationDate=" + publicationDate +
                '}';
    }

}
