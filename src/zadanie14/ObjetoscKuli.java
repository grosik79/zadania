package zadanie14;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ObjetoscKuli {
    public static void main(String[] args) throws IOException {

        double r,objetosc;
        System.out.println(" Podaj prosze promień kuli, r =  ");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        r = Double.parseDouble(bufferedReader.readLine());
        objetosc = 4 * Math.PI * r * r * r /3;

        System.out.printf("Objętość kuli o promieniu " + r + " wynosi: " + "%2.2f\n", objetosc);

    }
}
