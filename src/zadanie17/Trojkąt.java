package zadanie17;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Trojkąt {
    public static void main(String[] args) throws IOException {
        int a,b,c;
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println(" Podaj proszę długość przyprostokątnej - bok a: ");
        a = Integer.parseInt(bufferedReader.readLine());
        System.out.println(" Podaj proszę długość przyprostokątnej - bok b: ");
        b = Integer.parseInt(bufferedReader.readLine());
        System.out.println(" Podaj prosze przeciwprostokątną - bok c: ");
        c = Integer.parseInt(bufferedReader.readLine());

        if ((a*a + b*b) == c * c ){
            System.out.println(" Boki o wymiarach przyprostokątnych a: " + a + " b: " + b + " i przeciwprostąkątnej o wymiarze " + c + " tworzą trójkąt prostokątny");
        } else{
            System.out.println(" Boki o wymiarach przyprostokątnych a: " + a + " b: " + b + " i przeciwprostąkątnej o wymiarze " + c + " nie tworzą trójkąta prostokątnego");
        }
    }
}
